﻿using qedge.tac.local.Models;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qedge.tac.local.Controllers
{
    public class QedgeIntroController : Controller
    {
        // GET: QedgeIntro
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        private static QedgeIntro CreateModel()
        {
            var item = RenderingContext.Current.Rendering.Item;
            var qedgeIntro = new QedgeIntro()
            {
                Intro = new HtmlString(FieldRenderer.Render(item,"ContentIntro")),
                Image = new HtmlString(FieldRenderer.Render(item,"Qedge Image","mw=400"))
            };
            return qedgeIntro;
        }
    }
}