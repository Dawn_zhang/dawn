﻿using qedge.tac.local.Models;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qedge.tac.local.Controllers
{
    public class SlideIntroController : Controller
    {
        // GET: SlideIntro
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        private static IEnumerable<SlideIntro> CreateModel()
        {
            var item = RenderingContext.Current.Rendering.Item;
            var childrenItem = item.GetChildren()
                .FirstOrDefault(i => i.Name == "Qedge")
                .GetChildren()
                .FirstOrDefault(i => i.Name == "SlideIntro")
                .GetChildren();
            IEnumerable<SlideIntro> slideIntro = childrenItem.Select(s => new SlideIntro
            {
                Title = new HtmlString(FieldRenderer.Render(s, "ContentHeading")),
                Intro = new HtmlString(FieldRenderer.Render(s, "ContentIntro")),
                More = new HtmlString(FieldRenderer.Render(s, "MetaDescription")),
                Url = new HtmlString(FieldRenderer.Render(s, "Url")).ToString(),
                Image = new HtmlString(FieldRenderer.Render(s,"Qedge Image"))
            });
            return slideIntro;
        }
    }
}