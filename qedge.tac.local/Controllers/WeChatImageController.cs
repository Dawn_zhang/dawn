﻿using qedge.tac.local.Models;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qedge.tac.local.Controllers
{
    public class WeChatImageController : Controller
    {
        // GET: WeChatImage
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        private static Photo CreateModel()
        {
            var item = RenderingContext.Current.Rendering.Item;
            var image = new Photo
            {
                TypeImage = new HtmlString(FieldRenderer.Render(item, "TypeImage", "class=type")),
                CodeImage = new HtmlString(FieldRenderer.Render(item, "CodeImage", "class=code")),
                Intro = new HtmlString(FieldRenderer.Render(item, "Intro"))
            };
            return image;
        }
    }
}