﻿using qedge.tac.local.Models;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qedge.tac.local.Controllers
{
    public class BoxImageController : Controller
    {
        // GET: Image
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        private static Photo CreateModel()
        {
            var item = RenderingContext.Current.Rendering.Item;
            var image = new Photo
            {
                DesktopImage = new HtmlString(FieldRenderer.Render(item, "DesktopImage", "class=visible-on-desktop")),
                MobileImage = new HtmlString(FieldRenderer.Render(item, "MobileImage", "class=visible-on-mobile")),
                Intro = new HtmlString(FieldRenderer.Render(item, "Intro"))
            };
            return image;
        }
    }
}