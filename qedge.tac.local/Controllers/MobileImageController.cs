﻿using qedge.tac.local.Models;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qedge.tac.local.Controllers
{
    public class MobileImageController : Controller
    {
        // GET: MobileImage
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        private static IEnumerable<Photo> CreateModel()
        {
            var item = RenderingContext.Current.Rendering.Item;
            var childrenItem = item.GetChildren()
                .FirstOrDefault(i => i.Name == "Qedge")
                .GetChildren()
                .FirstOrDefault(i => i.Name == "WeChatImage")
                .GetChildren();
            IEnumerable<Photo> photoList = childrenItem.Select(s => new Photo
            {
                TypeImage = new HtmlString(FieldRenderer.Render(s, "TypeImage", "type")),
                CodeImage = new HtmlString(FieldRenderer.Render(s, "CodeImage", "code")),
                Intro = new HtmlString(FieldRenderer.Render(s, "Intro"))
            });
            return photoList;
        }
    }
}