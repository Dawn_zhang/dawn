﻿using qedge.tac.local.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qedge.tac.local.Controllers
{
    public class NavigationController : Controller
    {
        // GET: Navigation
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        public IEnumerable<NavigationItem> CreateModel()
        {
            var currentItem = RenderingContext.Current.ContextItem;
            var childrenItem = currentItem.GetChildren()
                                          .Where(i => i.TemplateName == "Qedge Details");
            List<Item> item = new List<Item>();
            item.Add(currentItem);
            foreach (var obj in childrenItem)
            {
                item.Add(obj);
            }
            IEnumerable<NavigationItem> NavigationList = item.Select(s => new NavigationItem
            {
                Title = (new HtmlString(FieldRenderer.Render(s, "ContentHeading"))).ToString(),
                URL = (new HtmlString(FieldRenderer.Render(s, "Url"))).ToString()
            });

            return NavigationList;
        }
    }
}