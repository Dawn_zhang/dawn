﻿using qedge.tac.local.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qedge.tac.local.Controllers
{
    public class PartnerController : Controller
    {
        // GET: Footer
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        private static IEnumerable<Partner> CreateModel()
        {
            var item = RenderingContext.Current.Rendering.Item;
            //var currentItem = RenderingContext.Current.ContextItem;
            var childrenItem = item.GetChildren()
                .FirstOrDefault(i => i.TemplateName == "Qedge Section")
                .GetChildren()
                .FirstOrDefault(i => i.Name == "Parner")
                .GetChildren();
            IEnumerable<Partner> partner = childrenItem.Select(s => new Partner
            {
                Image = new HtmlString(FieldRenderer.Render(s, "Qedge Image", "mw=400"))
            });
            return partner;
        }
    }
}