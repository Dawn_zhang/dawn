﻿using qedge.tac.local.Models;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace qedge.tac.local.Controllers
{
    public class FeaturedEventController : Controller
    {
        // GET: FeaturedEvent
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        private static FeaturedEvent CreateModel()
        {
            var item = RenderingContext.Current.Rendering.Item;
            var featuredEvent = new FeaturedEvent()
            {
                Heading = new HtmlString(FieldRenderer.Render(item, "ContentHeading")),
                QedgeImage = new HtmlString(FieldRenderer.Render(item, "Qedge Image")),
                Intro = new HtmlString(FieldRenderer.Render(item, "ContentIntro")),
                More = new HtmlString(FieldRenderer.Render(item, "MetaDescription")),
                URL = LinkManager.GetItemUrl(item)
            };
            return featuredEvent;
        }
    }
}