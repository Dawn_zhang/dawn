$(document).ready(function() {
    var $qedgeSlider = $("#lightSlider").lightSlider({
        controls: false,
        item: 1,
        auto: true,
        loop: true,
        pager: false,
        speed: 1000,
        pause: 7000,
        slideMargin: 0,
        enableTouch:true,
        onBeforeSlide: function(el) {
            var slideIndex = el.getCurrentSlideCount();
            $('#slide-intros > div').eq(slideIndex - 1).addClass('active').siblings().removeClass('active');
        }
    });


    $('#slider-prev').click(function() {
        $qedgeSlider.goToPrevSlide();
    });
    $('#slider-next').click(function() {
        $qedgeSlider.goToNextSlide();
    });


    $('#menu-toggle').click(function() {
        $('#menu').addClass('active');
        $('html').addClass('qedge-menu-opened');
    });

    $('#menu').on('click', 'a', function() {
        $('#menu').removeClass('active');
        $('html').removeClass('qedge-menu-opened');
    });


    var deviceTolerance = $(window).width() > 576 ? 0 : 90;
    var moveTo   = new MoveTo({
        tolerance: deviceTolerance,
        duration: 1000,
        easing: 'easeOutQuart'
    });
    $.each($('.js-trigger'), function(index, item) {
        moveTo.registerTrigger(item);
    });


    // var time;
    // var timeStamp  = 0;
    var $headerBg  = $('#header-bg');
    var scrolled   = 0;
    $(window).scroll(function() {
        // time	 = new Date();
        // time	 = time.getMilliseconds();
        // if (Math.abs(time - timeStamp) > 0) {
        // 	timeStamp = time;
            scrolled = $(document).scrollTop();
            if (scrolled <= 10) {
                $headerBg.css({'opacity': 0});
            } else if (scrolled > 10 && scrolled < 300) {
                $headerBg.css({'opacity': scrolled / 3 / 100});
            } else {
                $headerBg.css({'opacity': 1});
            }
        // }
    });
});