﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qedge.tac.local.Models
{
    public class Photo
    {
        public Photo() { }

        public HtmlString DesktopImage { get; set; }

        public HtmlString MobileImage { get; set; }

        public HtmlString TypeImage { get; set; }

        public HtmlString CodeImage { get; set; }

        public HtmlString PImage { get; set; }

        public HtmlString Intro { get; set; }
    }
}