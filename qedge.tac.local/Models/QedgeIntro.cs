﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qedge.tac.local.Models
{
    public class QedgeIntro
    {
        public QedgeIntro() { }

        public HtmlString Intro { get; set; }

        public HtmlString Image { get; set; }
    }
}