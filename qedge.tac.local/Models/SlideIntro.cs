﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qedge.tac.local.Models
{
    public class SlideIntro
    {
        public SlideIntro() { }

        public HtmlString Title { get; set; }

        public HtmlString Intro { get; set; }

        public HtmlString More { get; set; }

        public string Url { get; set; }

        public HtmlString Image { get; set; }
    }
}