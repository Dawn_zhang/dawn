﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qedge.tac.local.Models
{
    public class FeaturedEvent
    {
        public FeaturedEvent()
        {

        }
        public HtmlString Heading
        {
            get;
            set;
        }
        public HtmlString Intro
        {
            get;
            set;
        }
        public HtmlString More
        {
            get;
            set;
        }
        public HtmlString QedgeImage
        {
            get;
            set;
        }
        public string URL
        {
            get;
            set;
        }
    }
}